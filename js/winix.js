$(function () {
    selectFn();
    lnbFn();
    accordion();
    cityAccordion();
    listControl();
    listControlType();
    fadinSlide();
    dataSelect();
    layoutAccrodion();


    //     // 임시입니다. 개발시 삭제 부탁 드려요.
    $.ajax({
        url: "../header.html",
        success: function (data) {
            $(".wrapper").prepend(data);
            lnbFn();
            selectFn();
        }
    });
    //     // /임시입니다. 개발시 삭제 부탁 드려요.men
    //     //datepicker
    if ($(".calendar.input").length != 0) {
        jQuery.datetimepicker.setLocale("ko");
        $(".calendar.input").each(function () {
            var $this = $(this);
            if ($this.hasClass("seconds")) {
                $(".calendar.seconds input").datetimepicker({
                    format: 'Y-m-d h:m:s '
                });
            } else if ($this.hasClass("only")) {
                $(".calendar.input.only").datetimepicker({
                    timepicker: false
                });
            } else {
                $this.datetimepicker({
                    format: "Y-m-d H:i"
                });
            }
        })
        //         //오늘 click event
        //         $.datepicker._gotoToday = function(id) {
        //             $(id).datepicker('setDate', new Date()).datepicker('hide').blur();
        //         };
    }
});

function lnbFn() {
    var $lnb = $(".nav"),
        $menu = $lnb.find("li a");

    $menu.on("click", function (e) {
        // e.preventDefault();
        var $this = $(this),
            $parent = $this.parent("li");
        if ($lnb.hasClass("on")) {
            $lnb.removeClass("on");
        } else {
            if ($parent.hasClass("on")) {
                $parent.removeClass("on");
            } else {
                $parent
                    .addClass("on")
                    .siblings()
                    .removeClass("on");
            }
        }
    });
}

function dataSelect() {
    $("[data-select]").on('click', function () {
        var $target = $(this).data("select");
        $("[data-target=" + $target + "]").addClass('active');
    })
    var $closeBtn = $('.pop-close');
    $closeBtn.on('click', function () {
        $(this).parents('.pop').removeClass('active');
    })
}

function accordion() {
    var $selector = $('.accordion-set a.accordion');
    $selector.on('click', function () {
        $(this).parent().toggleClass('active');
    })
}

function listControlType() {
    var $selector = $('.list-accordion-type02 .list-control');
    $selector.on('click', function () {
        $(this).parent().toggleClass('active');
    })
}

function listControl() {
    var $selector = $('.list-accordion .list-control'),
        $all = $('.all-control');
    $all.add($selector).on('click', function () {
        var $this = $(this);
        if ($this.is($selector)) {
            $(this).parent().toggleClass('active');
        } else {
            if ($this.hasClass("active")) {
                $('.list-accordion').removeClass("active");
                $this.removeClass("active");
            } else {
                $this.addClass("active");
                $('.list-accordion').addClass("active");
            }
        }
    })
}


function cityAccordion() {
    var $selector = $('.city-accordion-wrapper .button-group');
    $selector.on("click", function () {
        $('.city-accordion-wrapper').toggleClass("active");
    });
}

function layoutAccrodion() {
    var $selector = $('.layout-accrodion a');
    $selector.on('click', function () {
        $(this).parent().toggleClass('active');
    })
}

function fadinSlide() {
    var $slideWrap = $('.slide-wrap'),
        $control = null;
    for (var i = 0; i < $slideWrap.length; i++) {
        $count = 0;
        var $control = $slideWrap.eq(i).find('.control-btn button'),
            $slide = $slideWrap.eq(i).find('.slide li'),
            $length = $slide.length;
        if (length === 1) {
            $control.parent().addClass('hidden');
        }
        slideBtn($control, $count, $length, $slide);

    }

    function slideBtn(btn, count, length, slide) {
        var $btn = btn,
            $length = length,
            $slide = slide,
            $count = count;
        $btn.on('click', function () {
            $(this).is('.next') ? $count++ : $count--;
            if ($count >= $length) {
                $count = 0;
            } else if ($count == -1) {
                $count = $length - 1;
            }
            $slide.eq($count).addClass('active').siblings().removeClass('active');
        })
    }

}

function selectFn() {
    var $select = $(".select"),
        $target;
    $select
        .children()
        .not("select")
        .remove();
    $(document)
        .off("click")
        .on("click", function (e) {
            var $target = $(e.target);
            if (!$target.is(".select, .selected,.select-list")) {
                $select.removeClass("on");
            }
        });
    $select.each(function () {
        var $this = $(this),
            $orgSelect = $this.children("select"),
            $orgData = $orgSelect.attr("name"),
            $orgOption = $orgSelect.children("option"),
            $selected = $("<div class='selected'></div>"),
            $selectList = $("<ul class='select-list'></ul>");
        $this.append($selected, $selectList);
        $orgOption.each(function (idx) {
            var $selectItem = $("<li class='select-item'></li>");
            if ($(this).filter(":selected").length != 0) {
                $selectItem = $("<li class='select-item on'></li>");
                $selected.text($(this).text());
            }
            $selectItem.text($(this).text());
            $selectList.append($selectItem);
        });
    });

    $($select)
        .off()
        .on("click", function () {
            if ($(this).hasClass("disabled")) {
                return;
            }
            var $this = $(this),
                $list = $this.children(".select-list"),
                $item = $this.find(".select-item"),
                $max = 0,
                $text = $this.children(".selected");
            $this.children("select").focusin();
            // if ($this.offset().top > $(window).height() / 2) {
            //     $this.addClass("revert");
            // }
            if ($this.hasClass("on")) {
                $select.removeClass("on");
                $this.add($list).removeAttr("style");
            } else {
                $select
                    .removeClass("on")
                    .find(".select-list")
                    .removeAttr("style");
                $this.addClass("on");
            }
            // $item.each(function() {
            //     var $text = $(this).text(),
            //         $textCount = 0;
            //     for (var i = 0; i < $text.length; i++) {
            //         if (/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/gi.test($text.charAt(i))) {
            //             $textCount += 2;
            //         } else {
            //             $textCount += 1;
            //         }
            //     }
            //     if ($max < $textCount) {
            //         $max = $textCount;
            //     }
            // });
            // if ($this.width() < $max * 9.8) {
            //     $this.add($list).css({
            //         width: $max * 9.8 + 8
            //     });
            // }

            $list.on("mouseleave", function () {
                $this.removeClass("on").removeAttr("style");
                $this.children("select").focusout();
            });
            $item.off().on("click", function (e) {
                var $itemThis = $(this),
                    $idx = $itemThis.index(),
                    $option = $this.find("option");
                $text.addClass("active");
                $text.attr("id", $option.eq($idx).val()); //190508
                $option
                    .eq($idx)
                    .prop("selected", true)
                    .siblings($option)
                    .prop("selected", false);
                $text.text($itemThis.text());
                $this.removeAttr("style");
                $itemThis
                    .addClass("on")
                    .siblings()
                    .removeClass("on");
                $option.parent().trigger("change");
                $this.children("select").focusout();
                setTimeout(function () {
                    $select.removeClass("on");
                }, 100);
            });
        });
}