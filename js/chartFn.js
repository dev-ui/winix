var randomScalingFactor = function() {
    return Math.round(Math.random() * 100);
};
var randomScalingFactor2 = function(max, min) {
    return Math.random() * (max - min) + min;


};
var config = [{
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                ],
                backgroundColor: [
                    "#8fc31f",
                    "#ededed",
                ],
            }],
            labels: [
                '디바이스 연결',
                '디바이스 미연결',
            ]
        },
        options: {
            plugins: {
                labels: [{
                    render: function(args) {
                        return args.percentage + "%" + " \n\n";
                    },
                    fontSize: 24,
                    fontColor: ['#fff', '#000'],
                    fontStyle: 'bold',
                }, {
                    render: function(args) {
                        return args.label;
                    },
                    position: 'border',
                    fontSize: 14,
                    fontColor: ['#fff', '#000'],

                }]
            },
            layout: {
                padding: {
                    left: 100,
                    right: 100,
                    top: 80,
                    bottom: 20
                }
            },
            radius: 4,
            rotation: 1 * Math.PI,
            circumference: 1 * Math.PI,
            responsive: true,

            legend: {
                position: 'bottom',
                labels: {
                    padding: 30,
                    fontSize: 14,
                    fontColor: "#333",
                    fontStyle: 600,
                    usePointStyle: true,
                },
            },
            title: {
                display: false,
                text: 'Chart.js Doughnut Chart'
            },
        }
    }, {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                ],
                backgroundColor: [
                    "#818980",
                    "#8fc31f",
                    "#f94439",
                    "#fdad01",
                    "#00a7fd",
                ],
            }],
            labels: [
                '0~60%',
                '61~70%',
                '71~80%',
                '81~90%',
                '91~100%',
            ]
        },
        options: {
            plugins: {
                labels: [{
                    render: function(args) {

                        return args.percentage + "%" + "\n\n"
                    },
                    fontSize: 20,
                    fontColor: '#fff',
                    fontStyle: 'bold',

                }, {
                    render: function(args) {
                        var text = args.label;
                        return text
                    },
                    fontSize: 14,
                    fontColor: '#fff',

                }]
            },
            layout: {
                padding: {
                    left: 00,
                    right: 00,
                    top: 50,
                    bottom: 20
                }
            },
            legend: {
                position: 'bottom',
                labels: {
                    padding: 20,
                    fontSize: 12,
                    fontColor: "#333",
                    fontStyle: 600,
                    usePointStyle: true,
                },
            },
            title: {
                display: false,
                text: 'Chart.js Doughnut Chart'
            },
        }
    }, {
        type: 'bar',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],

            datasets: [{
                    type: 'line',
                    fill: 'false',
                    data: [
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                    ],
                    backgroundColor: "#6c716b",
                    borderColor: '#6c716b',
                    borderWidth: 1,
                    label: 'data1',
                }, {
                    type: 'bar',
                    data: [
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                    ],
                    backgroundColor: "#00a7fd",
                    label: 'data2',
                }, {
                    type: 'bar',
                    data: [
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                    ],
                    backgroundColor: "#fdad01",
                    label: 'data2',
                },

            ],
        },
        options: {
            plugins: {
                labels: [{
                    render: function(args) {
                        return "";
                    },

                }]
            },
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 74,
                    bottom: 20
                }
            },
            radius: 4,
            responsive: true,

            legend: {
                position: 'bottom',
                labels: {
                    padding: 30,
                    fontSize: 14,
                    fontColor: "#333",
                    fontStyle: 600,
                    usePointStyle: true,
                },
            },

            title: {
                display: false,
                text: 'Chart.js Doughnut Chart'
            },
        }
    }, {
        type: 'bar',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],

            datasets: [{
                    type: 'line',
                    fill: 'false',
                    data: [
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                    ],
                    backgroundColor: "#6c716b",
                    borderColor: '#6c716b',
                    borderWidth: 1,
                    label: 'data1',
                }, {
                    type: 'bar',
                    data: [
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                    ],
                    backgroundColor: "#00a7fd",
                    label: 'data2',
                }, {
                    type: 'bar',
                    data: [
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                        randomScalingFactor2(100, -100),
                    ],
                    backgroundColor: "#fdad01",
                    label: 'data3',
                },

            ],
        },
        options: {
            plugins: {
                labels: [{
                    render: function(args) {
                        return "";
                    },

                }]
            },
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 74,
                    bottom: 20
                }
            },
            radius: 4,
            responsive: true,

            legend: {
                position: 'bottom',
                labels: {
                    padding: 30,
                    fontSize: 14,
                    fontColor: "#333",
                    fontStyle: 600,
                    usePointStyle: true,
                },
            },

            title: {
                display: false,
                text: 'Chart.js Doughnut Chart'
            },
        }
    },

    {
        type: 'bar',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],

            datasets: [{
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                ],
                backgroundColor: "#444f55",
                label: 'Android',
                stack: "stack2",
            }, {
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                ],
                backgroundColor: "#0cc5b7",
                label: 'IOS',
                stack: "stack2",
            }],
        },
        options: {
            plugins: {
                labels: [{
                    render: function(args) {
                        return "";
                    },

                }]
            },
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 74,
                    bottom: 10
                }
            },
            radius: 4,
            responsive: true,

            legend: {
                position: 'bottom',
                labels: {
                    padding: 30,
                    fontSize: 14,
                    fontColor: "#333",
                    fontStyle: 600,
                    usePointStyle: true,
                },
            },

            title: {
                display: false,
                text: 'Chart.js Doughnut Chart'
            },
        }
    }, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                ],
                backgroundColor: [
                    "#8fc31f",
                    "#ededed",
                ],
            }],
            labels: [
                'Login',
                'No-Login',
            ]
        },
        options: {
            plugins: {
                labels: [{
                    render: function(args) {
                        return args.percentage + "%" + " \n\n";
                    },
                    fontSize: 20,
                    fontColor: ['#fff', '#000'],
                    fontStyle: 'bold',
                    position: 'border',
                }, {
                    render: function(args) {
                        return args.label;
                    },
                    position: 'border',
                    fontSize: 12,
                    fontColor: ['#fff', '#000'],
                }]
            },
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 30,
                    bottom: 10
                }
            },
            radius: 4,
            responsive: true,

            legend: {
                position: 'bottom',
                labels: {
                    padding: 30,
                    fontSize: 14,
                    fontColor: "#333",
                    fontStyle: 600,
                    usePointStyle: true,
                },
            },
            title: {
                display: false,
                text: 'Chart.js Doughnut Chart'
            },
        }
    },
];
window.onload = function() {
    var el = ['item-status-pie', 'filter-status', 'item-status-bar', 'model-status-bar', 'month-mobile-status', 'last-login'],
        ctx = [];
    for (var i = 0; i < el.length; i++) {
        ctx[i] = document.getElementById(el[i]).getContext('2d');
        window.mychart = new Chart(ctx[i], config[i]);
    }


}